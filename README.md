# MangoPay Assignment

<em> Note - Please contact me if you're interested seeing more code. I'm happy to share the full project if/when I get hired. </em>

## Purpose
The goal of this project is to profile a given code sample, optimise it and monitor the effect.

## Setting Up Environment
### Java Spring Boot

I've extended the given code and created a fully functioning app I could test with. It's a typical Spring Boot, with a couple of Controllers, Services and Repositories (JPA). I went for the "bonus" incentive and added Exception handling and a simple Logger.

In addition to the `Products` entity (which I renamed `Items`) I also added `Users`. I wanted to create more load using more complex queries. 

Example of a Controller:

```
@RestController
@RequestMapping("/users")
public class UserController {
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;
    @Autowired
    private ItemService itemService;
    
    @Autowired
    protected ModelMapper pojoMapper;
    
    /*
     *  User Controller
     */
    
    // POST: http://localhost:8080/users
    // Content-Type: application/json
    // Payload: { "firstname": "Mick", "lastname": "Jagger" }
    @RequestMapping(value="", method=RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        LOGGER.debug("Received request to create the {}", user);
        return userService.save(user);
    }
    ...
}
```


Item Repository:

```
public interface ItemRepository extends JpaRepository<Item, Long> {

    @Query("SELECT i FROM Item i WHERE i.user.userId = ?1")
    List<Item> findByUserId(long userId);
}
```


Item Service:

```
@Service
@Validated
public class ItemServiceImpl implements ItemService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemServiceImpl.class);
    
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public List<Item> save(@NotNull @Valid final List<Item> items, @NotNull @Valid final long userId) {
        LOGGER.debug("Creating {} on userId={}", items, userId);

        long start_time = System.nanoTime();

        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new NoUserExistsException (
                    String.format("No user exists with id=%d", userId));
        }
        List<Item> itemsWithUser = new ArrayList<Item>();
        for (Item item : items) {
            if (itemRepository.exists(item.getItemId())) {
                throw new ItemAlreadyExistsException (
                        String.format("There already exists an item with id=%d", item.getItemId()));
            }
            item.setUser(user);
            itemsWithUser.add(item);
        }

        long end_time = System.nanoTime();
        double difference = (end_time - start_time) / 1e6;
        System.out.println("add_items_for_users query time: " + difference);

        return itemRepository.save(itemsWithUser);
    }
    ...
}
```


### MySQL

I've generated a large dataset of random `Users` and `Items` in JSON format to test with.
 
Example of payload:

```
[
    {"firstname": "Jessica", "lastname": "Young"},
    {"firstname": "Thomas", "lastname": "Brown"},
    {"firstname": "Karen", "lastname": "Davis"},
    {"firstname": "Daniel", "lastname": "Miller"},
    {"firstname": "Nancy", "lastname": "Thompson"},
    ...
]
```

### DBeaver v23.0.4

What a fantastic tool, always enjoying working with it. DBeaver provides a *free* GUI for databases with tonnes of other tools. In this project I used it for monitoring, running queries and looking into datasets.

### JProfiler v14.0

I've been using JProfiler for many years in other projects. I didn't get to proparly use it here as I wasn't looking into the JVM. I mention it as another tool in our arsenal. Given more time and scope I'd be using it too.

<br/>

## Creating Data
I've written a set of bash scripts to create ~100,000 records on both tables (`items` and `users`) and automated a few basic SQL ops. I'll share a few snippets a bit later.



## Testing Code & Monitoring

For running tests I've been using CURL with a random set of inputs. I've tried to create a script for each test with a few variables so I can cover a number of use cases.

A couple of examples:

```
#!/bin/bash

# JSON datasets containing the records
JSON_FILE="users_data.json"
# Define the base URL for the app
BASE_URL="http://localhost:8080"

# Loop through each JSON record and send a POST request
cat "$JSON_FILE" | while IFS= read -r record; do
    # Send a PUT request with curl
    curl -X POST -H "Content-Type: application/json" -d "$record" "$BASE_URL/users"
    #echo $record
done
```

```
#!/bin/bash

# Hard coded testing params; could pass at runtime via cmd
# limit the amount of users:
USER_ID=100
for ((i = 1; i <= 1000; i++)); do

    # Generate a random number between 1 and $USER_ID
    random_number=$(shuf -i 1-$USER_ID -n 1)

    curl -X POST -H "Content-Type: application/json" -d '[{ "itemName": "Java Tutorial Book", "quantity": 3 },{ "itemName": "Concert Tickets", "quantity": 2 }]' http://localhost:8080/users/$random_number/items

done
```


### Identify slow queries
Not much use here (we're working with simple queries in this project) but a profiling option worth mentioning. On its own the slow query log is not *very* helpful so should be used alongside tools for analysing logs such as Logstash or Splunk.

Regardless, I enabled the slow queries log in MySQL and set it to a low threshold: 

```
set global slow_query_log = 'ON';
set global slow_query_log_file ='./slow-query.log';
set global long_query_time = 1;
show variables like '%slow%';
```


output example:
```
# Time: 2023-10-02T07:15:43.252134Z
# User@Host: demo[demo] @ localhost [127.0.0.1]  Id:  1537
# Query_time: 0.000590  Lock_time: 0.000001 Rows_sent: 0  Rows_examined: 0
SET timestamp=1696230943;
/* ApplicationName=DBeaver 23.0.4 - Metadata */ SELECT cc.CONSTRAINT_NAME, cc.CHECK_CLAUSE, tc.TABLE_NAME
FROM (SELECT CONSTRAINT_NAME, CHECK_CLAUSE
FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS
WHERE CONSTRAINT_SCHEMA = 'demo'
ORDER BY CONSTRAINT_NAME) cc,
(SELECT TABLE_NAME, CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE TABLE_SCHEMA = 'demo' AND TABLE_NAME='user') tc
WHERE cc.CONSTRAINT_NAME = tc.CONSTRAINT_NAME
ORDER BY cc.CONSTRAINT_NAME;
# Time: 2023-10-02T07:15:43.256548Z
# User@Host: demo[demo] @ localhost [127.0.0.1]  Id:  1537
# Query_time: 0.003840  Lock_time: 0.000002 Rows_sent: 0  Rows_examined: 630
SET timestamp=1696230943;
```

<br>

### Explain Plans

It's a little difficult to see anything outstanding using the `EXPLAIN` and `EXPLAIN ANALYZE` commands. The dataset and schema aren't complex nor large enough. Given more time to work on this, I'd write a few possible `WHERE`,`ORDER BY` clauses and `JOIN` conditions. Can then look into indexes and performance during `INSERT`, `UPDATE` and `DELETE` calls.

However, the option is there and easy to work with on DBeaver

`EXPLAIN SELECT * FROM `user` u, item i 
where i.user_id = u.user_id ;`

<br>

## Optimise Environment, Test & Monitor

### Caching
I've ran a few tests using both `javax.cache.cache-api` & `org.ehcache.ehcache` as app level caching. 

Examples:

```
@Service
@Validated
@CacheConfig(cacheNames = "users") // optimisation
public class UserServiceImpl implements UserService {

...

    @Override
    @Transactional(readOnly = true)
    @CacheEvict(key = "#id")
    @Cacheable(key = "#id") // optimisation
    public User findOne(long id) {
        LOGGER.debug("Retrieving a user by user id={}", id);

        long start_time = System.nanoTime();

        User user = userRepository.findOne(id);
        if (user == null) {
            throw new NoUserExistsException(
                    String.format("No user exists with id=%d", id));
        }

        long end_time = System.nanoTime();
        double difference = (end_time - start_time) / 1e6;
        System.out.println("find_user_by_id query time: " + difference);
        
        return user;
    }

}
```

The test was mostly to retrieve user by id.

I then disabled the cache (it's enabled by default in JPA) for the whole project and rerun the test. Sample of results (timing would include network latency):


Cache Disabled:

```
find_user_by_id query time: 1.48494
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 0.979051
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.117157
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.305045
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.010853
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.34757
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.018033
2023-10-01 23:15:37 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:15:37 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.34704
```


Cache Enabled:

```
2023-10-01 23:17:04 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:17:04 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 0.916365
2023-10-01 23:17:04 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:17:04 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 0.946012
2023-10-01 23:17:04 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:17:04 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 0.90447
2023-10-01 23:17:04 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:17:04 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 1.022387
2023-10-01 23:17:04 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-01 23:17:04 DEBUG c.h.r.UserServiceImpl:55 - Retrieving a user by user id=4
find_user_by_id query time: 0.945761
```


No cache, different users

```
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=1
find_user_by_id query time: 1.38347
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/2
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=2
find_user_by_id query time: 1.137462
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/3
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=3
find_user_by_id query time: 0.912396
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/4
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=4
find_user_by_id query time: 1.002362
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/5
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=5
find_user_by_id query time: 1.119067
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/6
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=6
find_user_by_id query time: 1.771996
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/7
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=7
find_user_by_id query time: 1.218934
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/8
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=8
find_user_by_id query time: 0.956105
2023-10-02 00:03:55 INFO  c.h.c.AccessLog:23 - [GET] /users/9
2023-10-02 00:03:55 DEBUG c.h.r.UserServiceImpl:57 - Retrieving a user by user id=9
find_user_by_id query time: 0.914426
```



### Eager Fetching & Lazy Loading


The `Eager Fetching` technique involves using join fetch and batch size to minimize the number of queries sent to the database.

The `Lazy Loading` technique is simply delaying the loading of the needed data until it's actually needed. 
I've been using both of these techniques to observe any performance trends. I was expecting to note some extra load due to the `N+1` problem, but in reality the test isn't complex enough to make any significant improvements.

Code sample:  

```
@Entity
@Table(name="user")
@Data
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_id")
    private long userId;
    
    @Column(name="first_name", nullable=false, length=50)
    private String firstname;
    
    @Column(name="last_name", length=20)
    private String lastname;
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="user")
    private List<Item> items;

    // The default constructor only exists for the sake of JPA.
    // We won’t use it directly, so it is designated as protected.
    protected User() {}
}
```


And the `Item` entity:

```
@Entity
@Table(name="item")
@Data
public class Item {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="item_id")
    private long itemId;
    
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Column(name="item_name", nullable=false, length=50)
    private String itemName;
    
    @Column(name="quantity", nullable=false)
    private Integer quantity;

    ...

}

```


### Pagination (& Sorting, but without...)
This technique limits the amount of data retrieved from the database, improving response time for large returned lists.

Service:
```
    // with Pagination
    @Override
    @Transactional(readOnly = true)
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

```

Controller:
```
    // with Pagination
    // GET: http://localhost:8080/users
    public Page<UserResponse> getUsers(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {

        return userService.findAll( PageRequest.of(page, size) );
    }
```

## Results & Conclusions

Although some results are immediately obvious, other tests are needed with more time and effort.

Here's the DBeaver profiler before and after the optimisations mentioned above. Note the values of each column as the graphs scale to fit.

Optimised:

![optimised](images/optimised.jpg)

Not optimised:

![not_optimised](images/not_optimised.jpg)


Overall the above optimisations should only show significant results under more complex datasets and queries. With these the profiling would be a lot more interesting and meaningful too. I did, however, enjoy this assignment and hope to work on similar projects full time.



## Bonus: error handling

Example:

```
@ControllerAdvice
public class GlobalExceptionController {

    protected static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionController.class);

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleIllegalArgumentException(HttpServletRequest request, IllegalArgumentException e) {
        LOGGER.info("IllegalArgumentException occured: URL="+request.getRequestURL());
        return e.getLocalizedMessage();
    }
    
    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public String handleUserAlreadyExistsException(HttpServletRequest request, UserAlreadyExistsException e) {
        LOGGER.info("UserAlreadyExistsException occured: URL="+request.getRequestURL());
        return e.getLocalizedMessage();
    }
    
    @ExceptionHandler(NoUserExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleNonExistingUserException(HttpServletRequest request, NoUserExistsException e) {
        LOGGER.info("NoUserExistsException occured: URL="+request.getRequestURL());
        return e.getLocalizedMessage();
    }
    
    @ExceptionHandler(ItemAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public String handleItemAlreadyExistsException(HttpServletRequest request, ItemAlreadyExistsException e) {
        LOGGER.info("ItemAlreadyExistsException occured: URL="+request.getRequestURL());
        return e.getLocalizedMessage();
    }
    
    @ExceptionHandler(NoItemExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleNonExistingItemException(HttpServletRequest request, NoItemExistsException e) {
        LOGGER.info("NoItemExistsException occured: URL="+request.getRequestURL());
        return e.getLocalizedMessage();
    }
}```
